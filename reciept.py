#Course code :       SODV1101
#TERM/Year :         Fall2017
#Assignment code:    PA3
#Author :            Nico Beekhuijs
#BVC username :      n.beekhuijs948
#Date created :      2017-10-27
#Description :       Program source code

# program purpose

"""this program will create the sales reciept, broken down into meal price,
discount, tax, tip, total, and the total number of 20, 10, 5, and 1 $ bills to
"""

def create_reciept():

    """this function creates the reciept"""

    # input
    meal_price = float(input("Enter the meal price "))
    coupon_code = input("Enter the coupon code if you have one ")

    # processing
    discount_percentage = 0.1
    tax_rate = 0.05

    # apply discount if a coupon code was entered
    if coupon_code != '':
        discount = meal_price * discount_percentage
    else:
        discount = 0

    discount_price = meal_price - discount

    # calculate tax
    tax_amount = discount_price * tax_rate
    sub_total = discount_price + tax_amount

    # calculate tip
    if sub_total > 25.00:
        tip_percent = 0.22
    else:
        if sub_total > 15.00:
            tip_percent = 0.18
        else:
            if sub_total > 10.00:
                tip_percent = 0.15
            else:
                if sub_total >= 5.00:
                    tip_percent = 0.12
                else:
                    tip_percent = 0.10

    tip_amount = sub_total * tip_percent

    # calculate total
    grand_total = sub_total + tip_amount

    # output
    print("Reciept:")
    print("The meal price is $", meal_price)
    print("Coupon code is", coupon_code)
    print("The discount is $", -discount)
    print("The tax is $", tax_amount)
    print("The tip is $", round(tip_amount, 3))
    print("The total is $", grand_total)

    # breakdown in as few bills as posible
    bill_splitter(grand_total)


def bill_splitter(amount_to_split):
    """this splits the passed amount into 20, 10, 5, and 1$ bills"""
    bill_amount = 20
    while amount_to_split:
        bill_count = int(amount_to_split / bill_amount)
        amount_to_split = int(amount_to_split) - (bill_amount * bill_count)
        if bill_count:
            print(bill_count, "x "+str(bill_amount)+"s")
        if bill_amount%2:
            bill_amount = 2
        bill_amount = int(bill_amount / 2)


create_reciept()


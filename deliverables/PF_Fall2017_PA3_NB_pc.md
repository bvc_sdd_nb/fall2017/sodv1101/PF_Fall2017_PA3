Course code 	: SODV1101
Term/Year 	    : fall2017
Assignment code	: PA3
Author 		    : Nico Beekhuijs
BVC Username 	: n.beekhuijs948
Date created 	: 2017-10-16
Description 	: Algorithm description, IPO Chart and pseudocode


-- Algorithm Description --

Input of the program will be the meal price and coupon code (optional)
if a coupon code has been entered a 10% discount will be taken of the meal price

Next, the program will calculate the tax amount based on the meal price (5%)

The total of meal price and taxes will be used to determain the tip percentage according 
to the provided table.

The tip amount will be calculated with this percentage.

The total price (meal price + tax + tip) will be broken down into the least number of
bills and shown on the reciept


-- IPO Chart --

input         processing                                        output
-------------------------------------------------------------------------------
meal_price    discount = meal_price * discount_percent          meal_price
coupon_code   discount_price = meal_price - discount            coupon_code
              tax_amount = discount_price * tax_rate            discount
              sub_total = discount_price + tax_amount           tax_amount
              tip_amount = sub_total * tip_percent              tip_amount
              grand_total = sub_total + tip_amount              grand_total
              bills_twenties = grand_total / 20                 bills_twenties
              remainder = grand_total - (bills_twenties * 20)   bills_tens
              bills_tens = remainder / 10                       bills_fives
              remainder = remainder - (bills_tens * 10)         bills_ones
              bills_fives = remainder / 5
              remainder = remainder - (bills_fives * 5)
              bills_ones = remainder / 1
              
              
-- Pseudocode --

no pseudocode from me on this one. I hope I don't loose too many points for not
providing any. But I guess the bonus points for my awesome bill_splitter function
will even things out :)